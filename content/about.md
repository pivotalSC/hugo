---
title: About Us
subtitle: Our story
comments: false
---

PivotalSC is a scientific consulting company, started by two Sydney scientists, that provides solutions to complex problems.


### Peter

Peter is a multidisciplinary scientist. He has extensive experience in designing and executing experiments to gain a deeper understanding of systems to allow for their optimisation. Peter’s previous experience in the manufacturing sector has given him a keen ability to optimise manufacturing processes within tight constraints. He has additionally worked one-on-one with entrepreneurs to realise their scientific ideas and turn them into products. Peter has extensive experience writing technical documentation, communicating complex scientific concepts to people of varying backgrounds, and providing training to the next generation of scientists.

### Christopher

Christopher has a decade of experience developing and implementing solutions to technical problems in both research and industrial settings. In addition to being a highly capable synthetic and materials chemist, he is multidisciplinary, and has areas of specialisation as diverse as 3D printing, electronics prototyping, programming, automation, and data analysis. Christopher is a highly experienced laboratory manager. In this capacity, he has both optimised the operation of existing laboratories, and designed and established new research laboratories.
