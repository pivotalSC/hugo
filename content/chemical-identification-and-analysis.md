
---
title: Chemical Identification and Analysis
subtitle: Services offered
comments: false
---

We offer a range of chemical identification and analysis services that can be performed to meet any regulation requirements. Our services include but are not limited to:

* Audited sample collection
* Contaminant tracing
* Trade waste identification
* Sample analysis - FTIR, NMR, TGA, DSC
* Purification
* Thorough documentation


[Contact us](mailto:contact@pivotalsc.com) to discuss your needs
